<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@veriteworks.jp so we can send you a copy immediately.
 *
 * @category   tracking
 * @package    Veriteworks_Yjtracking
 * @copyright  Copyright (c) 2016 Veriteworks Inc. (http://veriteworks.co.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Veriteworks\Yjtracking\Helper;

use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package Veriteworks\Yjtracking\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getConversionIsActive($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/conversion/active', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getConversionId($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/conversion/id', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getConversionValue($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/conversion/value', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getYdnIsActive($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/ydn/active', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getYdnAccount($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/ydn/account', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getYdnLabel($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/ydn/label', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getUniversalIsActive($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/universal/active', $store);
    }


    /**
     * @param string $store
     * @return mixed
     */
    public function getUniversalAccount($store = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->
        getValue('veriteworks_yjtracking/universal/account', $store);
    }

}