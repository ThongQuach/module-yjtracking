<?php

namespace Veriteworks\Yjtracking\Test\Unit\Helper;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class DataTest
 * @package Veriteworks\Yjtracking\Test\Unit\Helper
 */
class DataTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Magento\Framework\App\Http\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_scopeMock;
    /**
     * @var \Magento\Framework\App\Http\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_helper;

    /**
     *
     */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->_scopeMock =
            $this->getMockBuilder('Magento\Framework\App\Config\ScopeConfigInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $contextMock = $this->getMockBuilder('Magento\Framework\App\Helper\Context')
            ->disableOriginalConstructor()
            ->getMock();
        $contextMock->expects($this->any())
            ->method('getScopeConfig')
            ->willReturn($this->_scopeMock);
        $this->_helper =
            $objectManager->getObject('Veriteworks\Yjtracking\Helper\Data',
                ['context'=>$contextMock]);
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getConversionIsActive
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetConversionIsActive()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('1');
        $this->assertEquals($this->_helper->getConversionIsActive(), '1');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getConversionId
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetConversionId()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('01234567890');
        $this->assertEquals($this->_helper->getConversionId(), '01234567890');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getConversionValue
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetConversionValue()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('01234567890');
        $this->assertEquals($this->_helper->getConversionValue(),
            '01234567890');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getYdnIsActive
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetYdnIsActive()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('1');
        $this->assertEquals($this->_helper->getYdnIsActive(), '1');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getYdnAccount
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetYdnAccount()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('01234567890');
        $this->assertEquals($this->_helper->getYdnAccount(), '01234567890');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getYdnLabel
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetYdnLabel()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('01234567890');
        $this->assertEquals($this->_helper->getYdnLabel(), '01234567890');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getUniversalIsActive
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetUniversalIsActive()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('1');
        $this->assertEquals($this->_helper->getUniversalIsActive(), '1');
    }

    /**
     * @covers Veriteworks\Yjtracking\Helper\Data::getUniversalAccount
     * @covers Veriteworks\Yjtracking\Helper\Data::getTestSender
     */
    public function testGetUniversalAccount()
    {
        $this->_scopeMock->expects($this->once())
            ->method('getValue')
            ->willReturn('01234567890');
        $this->assertEquals($this->_helper->getUniversalAccount(),
            '01234567890');
    }

}