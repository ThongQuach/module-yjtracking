<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@veriteworks.jp so we can send you a copy immediately.
 *
 * @category   tracking
 * @package    Veriteworks_Yjtracking
 * @copyright  Copyright (c) 2016 Veriteworks Inc. (http://veriteworks.co.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Veriteworks\Yjtracking\Block;

use Veriteworks\Yjtracking\Helper\Data;

/**
 * Class Universal
 * @package Veriteworks\Yjtracking\Block
 */
class Universal extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Veriteworks\Yjtracking\Helper\Data
     */
    protected $_helper;

    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Yjtracking::universal.phtml';

    /**
     * Universal constructor.
     * @param \Veriteworks\Yjtracking\Helper\Data $helper
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Data $helper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data
    ) {
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * @return \Veriteworks\Yjtracking\Helper\Data
     */
    public function getHelper()
    {
        return $this->_helper;
    }
}