<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@veriteworks.jp so we can send you a copy immediately.
 *
 * @category   tracking
 * @package    Veriteworks_Yjtracking
 * @copyright  Copyright (c) 2016 Veriteworks Inc. (http://veriteworks.co.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Veriteworks\Yjtracking\Block;

use Veriteworks\Yjtracking\Helper\Data;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

/**
 * Class Ydn
 * @package Veriteworks\Yjtracking\Block
 */
class Ydn extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Veriteworks\Yjtracking\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollection;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Yjtracking::ydn.phtml';


    /**
     * Ydn constructor.
     * @param \Veriteworks\Yjtracking\Helper\Data $helper
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollection
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Data $helper,
        CollectionFactory $salesOrderCollection,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data
    ) {
        $this->_helper = $helper;
        $this->_checkoutSession = $checkoutSession;
        $this->_orderCollection = $salesOrderCollection;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        $quoteId = $this->_checkoutSession->getLastQuoteId();
        $orders = $this->_orderCollection->create()
            ->addAttributeToFilter('quote_id', $quoteId)
            ->load();

        return $orders;
    }


    /**
     * @return \Veriteworks\Yjtracking\Helper\Data
     */
    public function getHelper()
    {
        return $this->_helper;
    }

}