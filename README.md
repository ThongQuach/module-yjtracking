# Magento2 extension for Yahoo! Japan Tracking.

## Overview
Magento2 extension for Yahoo! Japan Tracking.

## Installation
Install it with Aja:

    aja project add-module veriteworks/module-yjtracking:semantic.version.number`


# License

[Open Software License 3.0](http://opensource.org/licenses/osl-3.0.php)
[Academic Free License 3.0](http://opensource.org/licenses/afl-3.0.php)

# Copyright

(c) 2016 Veriteworks Inc.